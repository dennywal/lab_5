This was a lab in which I reduced an image captured from a CCD on a telescope to produce a usable science image.
I removed bias and noise from the original image displayed the raw and reduced images.
The lab is written in an ipython (jupyter) notebook and requires the numpy, scipy and matplotlib libraries.